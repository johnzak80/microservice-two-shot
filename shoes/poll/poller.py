import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO




def poll():
    while True:
        print('Shoes poller polling for data...')
        # Display a message to indicate that the poller is running and checking for data.

        try:
            response = requests.get('http://wardrobe-api:8000/api/bins/')
            # Send an HTTP GET request to the wardrobe API to retrieve data about bins.
            
            content = json.loads(response.content)
            # Parse the JSON response content into a Python dictionary.

            for bin in content["bins"]:
                # Iterate over each bin in the retrieved data.
                # print(bin)
                # # Print information about the bin to the console.

                BinVO.objects.update_or_create(
                    import_href=bin["href"],
                    defaults={
                        "closet_name": bin["closet_name"]
                    }
                )
                # Update or create a BinVO object in the database using information from the bin.

        except Exception as e:
            # Catch and handle any exceptions that may occur during the execution of the code.
            print(e, file=sys.stderr)
            # Print the exception message to the error stream (sys.stderr).

        time.sleep(60)
        # Sleep for 60 seconds before polling again. This controls the polling interval.

if __name__ == "__main__":
    poll()
# If this script is executed as the main program, start the polling process by calling the 'poll' function.

