from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "id"]
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer",
                  "model_name",
                  "color",
                  "picture_url",
                  "id",
                  "bin"]
    encoders = {
            "bin": BinVOEncoder()
        }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
            if bin_vo_id is not None:
                shoes = Shoe.objects.filter(bin=bin_vo_id)
            else:
                shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeDetailEncoder,
            )
    else:
        content = json.loads(request.body)
        print(BinVO.objects.all())
        try:
            bin_href = f'/api/bins/{bin_vo_id}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )

@require_http_methods("DELETE")
def api_show_shoe(request, id):
    
    # shoe = Shoe.objects.get(id=pk)
    # return JsonResponse(
    #     shoe,
    #     encoder=ShoeDetailEncoder,
    #     safe=False,
    # )


    count, _ = Shoe.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})

