from django.db import models

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    href = models.CharField(max_length=200, null=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=120)
    style_name = models.CharField(max_length=120)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT,
    )
