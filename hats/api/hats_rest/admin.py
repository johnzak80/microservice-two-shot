from django.contrib import admin
from .models import Hat, LocationVO

# Register your models here.


@admin.register(Hat)
class Hatdmin(admin.ModelAdmin):
    pass


@admin.register(LocationVO)
class LocationVOdmin(admin.ModelAdmin):
    pass
