from django.urls import path
from .api_views import list_hats, show_hat


urlpatterns = [
    path('list_hats/', list_hats, name='list_hats'),
    path('show_hat/<int:id>/', show_hat, name='show_hat'),
    path('delete_hat/<int:id>/', show_hat, name='delete_hat'),
]
