from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import LocationVO, Hat
import json

# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "href",
        "id"
    ]


class LocationDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "href",
        "id"
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "location",
        "picture_url",
        "id"
    ]
    encoders = {
        "location": LocationDetailEncoder(),
    }


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "picture_url",
        "id"
    ]


@require_http_methods(["GET", "DELETE"])
def show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            print('my hat: ', hat)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat Does Not Exist!"}
            )

    else:
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        print('my hat: ', hats)
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            loc_href = content["location"]
            print(content)
            location = LocationVO.objects.get(href=loc_href)
            content["location"] = location
            print(content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatDetailEncoder,
        safe=False,
    )
