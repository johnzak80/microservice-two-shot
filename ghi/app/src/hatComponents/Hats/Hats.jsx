import React, { useState, useEffect } from 'react';
import HatsList from "../HatsList"
import AddHatForm from "../AddHatForm"
import "./hats.css"

function Hats(props) {
    const [hatsList, setHatsList] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [locations, setLocations]= useState([]);
    const getHats = async () => {
        const hatsURL = 'http://localhost:8090/api/list_hats';
        const response = await fetch(hatsURL);
        if (response.ok) {
            const data = await response.json();
            console.log("hats data: ", data.hats);
            setHatsList(data.hats);

        } else {
            console.error(response);
        }
    }
    async function getLocations(){
        const locationURL = "http://localhost:8100/api/locations";
        const response = await fetch(locationURL);
        if (response.ok) {
            const data = await response.json();
            console.log("location data: ", data);
            setLocations(data.locations);

        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        console.log("HatsList component mounted");
        getLocations();
        getHats();
    }, [showForm]);

    if (showForm) {
        console.log("locations: ", locations)
        return (
            <div className="hatsDiv p-4">
                <div className='row'>
                    <h1 className="col text-light">Hats Closet</h1>
                    <h3 className="text-center text-light">Add a hat to closet</h3>
                </div>
                <div className="row justify-content-center">
                    <AddHatForm setShowForm = {setShowForm} hatsList={hatsList} setHatsList={setHatsList} locations={locations}/>
                </div>

            </div>
        )
    } else {
        return (
            <div className="hatsDiv p-4">
                <div className=''>
                    <h1 className="text-light">Hats Closet</h1>
                    <button className="btn btn-success" onClick={()=>setShowForm(true)}>Add a hat</button>
                </div>
                <div className="hatCards row">
                    <HatsList hatsList={hatsList} setHatsList={setHatsList} />
                </div>

            </div>

        )
    }


}
export default Hats
