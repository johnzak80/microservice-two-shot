import React, { useState, useEffect } from 'react';

function AddHatForm(props) {
    const emptyFields = {
        fabric: "",
        style_name: "",
        color: "",
        location: "",
        picture_url: ""
    }
    const [formFields, setFormFields] = useState(emptyFields)
    function handleFormChange(e) {
        let temp = { ...formFields };
        temp[e.target.id] = e.target.value;
        setFormFields(temp);
    }
    async function handleSubmit(e) {
        e.preventDefault();
        console.log("formdata", formFields);
        const createHatURL = "http://localhost:8090/api/list_hats/"
        const data = {...formFields}
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
               'Content-Type': 'application/json',
            },
         };
        const response = await fetch(createHatURL,fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log("newHat", newHat);
            setFormFields(emptyFields);
        }
        props.setShowForm(false);
    }
    return (
        <>
            <form className="form w-50 " onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    {/* <!-- Now, each field in our form references the same function --> */}
                    <input
                        className="form-control" placeholder="Style Name"
                        type="text"
                        name="style_name"
                        id="style_name"
                        value={formFields.style_name}
                        onChange={handleFormChange}
                        required />
                    <label htmlFor="style_name">Name</label>
                </div>

                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="Fabric"
                        type="text"
                        name="fabric"
                        id="fabric"
                        value={formFields.fabric}
                        onChange={handleFormChange}
                        required
                    />
                    <label htmlFor="fabric">Fabric</label>
                </div>

                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="Color"
                        type="text"
                        name="color"
                        id="color"
                        value={formFields.color}
                        onChange={handleFormChange}
                        required
                    />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" placeholder="Image URL"
                        type="text"
                        name="picture_url"
                        id="picture_url"
                        value={formFields.picture_url}
                        onChange={handleFormChange}
                        required
                    />
                    <label htmlFor="color">Image URL</label>
                </div>


                <div className="mb-3">
                    <select className="form-select"
                        name="location"
                        id="location"
                        value={formFields.location}
                        onChange={handleFormChange}
                        required>
                        <option value="">Choose a location</option>
                        {props.locations.map((location, idx) => {
                            return (
                                <option key={idx} value={location.href}>
                                    Closet{location.closet_name} : section {location.section_number} : shelf {location.shelf_number}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div>
                <button className="btn btn-primary float-start" onClick={handleSubmit}>Add Hat</button>
                <button className="btn btn-primary float-end" onClick={()=>props.setShowForm(false)}>return</button>
                </div>

            </form>
        </>
    )
}

export default AddHatForm;
