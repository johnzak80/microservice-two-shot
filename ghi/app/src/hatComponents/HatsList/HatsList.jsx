import React, { useState, useEffect } from 'react';



function HatsList({ hatsList, setHatsList}) {
    const [changeState, setChangeState] = useState(hatsList.length);
    async function deleteHat (hat) {
        console.log("hat.target.id.value:", hat.target.id);
        console.log("hat.target.value:", hat.target.value);
        const hatId = hat.target.id;
        const deleteURL = `http://localhost:8090/api/delete_hat/${hatId}/`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json'
            }
        }
        const response = await fetch(deleteURL, fetchConfig);
        let newState = hatsList.length-1;
        setChangeState(newState);

    }
    const getHats = async () => {
        const hatsURL = 'http://localhost:8090/api/list_hats';
        const response = await fetch(hatsURL);
        if (response.ok) {
            const data = await response.json();
            console.log("hats data: ", data.hats);
            setHatsList(data.hats);

        } else {
            console.error(response);
        }
    }
    useEffect(getHat => {
        console.log("props", hatsList)
        getHats();
    },[changeState])

    return (
        <>
            {(hatsList).map((hat,idx) => {
                return (
                    <div className="card" key={idx} >
                            <div className="card-body">
                                <div className="row">
                                <h5 className="col-8 card-title">{hat.style_name}</h5>
                                <img src={hat.picture_url} className="col card-img-top" alt="..." />
                                </div>

                            </div>
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item"><b>Closet: {hat.location.closet_name}</b></li>
                                <li className="list-group-item">Shelf #: {hat.location.shelf_number} - Section #: {hat.location.section_number}</li>
                                <li className="list-group-item">Item Id #: {hat.location.id}</li>
                            </ul>
                            <div className="card-body">
                                <button className="btn-sm btn-success card-link"
                                id={hat.id}
                                onClick = {(hat)=>deleteHat(hat)}
                                >Remove</button>
                                {/* <a href="#" data-hat={hat.id}className="btn-sm btn-success card-link">Move location</a> */}
                            </div>
                    </div>
                )

            })}
        </>
    )
}
export default HatsList;
