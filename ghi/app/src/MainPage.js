import image from './Wardrobe.png'

function MainPage() {
  return (
    <div className=" bg-md py-5">
    <div className="container text-center">
      <h1 className="display-4 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <h2 className="lead mb-4 fw-bold ">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </h2>
        <img src={image} alt="Image" className="img-fluid rounded-circle" />
      </div>
    </div>
  </div>
);
}

export default MainPage;
