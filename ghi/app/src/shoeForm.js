import React, { useEffect, useState } from "react";

function ShoeForm() {
  // Here is the old way of creating state hooks for every
  // property. Can you refactor this to make it into a single
  // data object like the ConferenceForm() above?
  const [bins, setBins] = useState([]);
  const [modelName, setModelName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [color, setColor] = useState("");
  const [pictureURL, setPictureURL] = useState("");
  const [bin, setBin] = useState("");


  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.model_name = modelName
    data.manufacturer = manufacturer;
    data.color = color;
    data.picture_url = pictureURL;
    data.bin = bin;
    
 

    const shoeUrl = `http://localhost:8080${bins[0].href}shoes/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

    setModelName("");
    setManufacturer("");
    setColor("");
    setPictureURL("");
    setBin("");
    }
  };

  // How can we refactor these handleChange methods to make
  // a single method, like the ConferenceForm above?
  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureURLChange = (event) => {
    const value = event.target.value;
    setPictureURL(value);
  };
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="bg-white">Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                value={modelName}
                onChange={handleModelNameChange}
                placeholder="modelName"
                required
                type="text"
                name="modelName"
                id="modelName"
                className="form-control"
              />
              <label htmlFor="modelName">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={manufacturer}
                onChange={handleManufacturerChange}
                placeholder="Manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={color}
                onChange={handleColorChange}
                placeholder="color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={pictureURL}
                onChange={handlePictureURLChange}
                placeholder="pictureUrl"
                required
                type="url"
                name="pictureUrl"
                id="pictureUrl"
                className="form-control"
              />
              <label htmlFor="pictureUrl">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                value={bin}
                onChange={handleBinChange}
                required
                name="bin"
                id="bin"
                className="form-select">
                <option value="">Choose a bin</option>
                {bins.map((bin,i) => {
                  return (
                    <option key={i} value={bin.import_href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
