import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

function ShoesList() {

  const [shoeList, setShoeList] = useState([]);

const loadShoes= async () => {
  const response = await fetch('http://localhost:8080/api/shoes');
  if (response.ok){
    const data = await response.json()
    setShoeList(data.shoes)
  }else{
    setShoeList([])
  }
}

useEffect(()=>{
  loadShoes()
}, [])

const deleteShoe = async (event) => {
  const shoeID = event.target.value;
  console.log("e:", event)
  const Url = `http://localhost:8080/api/shoes/${shoeID}`;
  const fetchConfig = {
    method: "delete",
  };
  const response = await fetch(Url, fetchConfig);
  if (response.status === 200) {
    const newShoeList = shoeList.filter((shoe) => shoe.id != shoeID);
    // console.log("newShoeList:", newShoeList);
    setShoeList(newShoeList);
  }
};

    return (
        
        <div className="row ">
             <div className="mt-2 mb-2">
                <Link to="new" className="btn btn-info btn-md ">
                Add A Shoe
                </Link>
            </div>

        {shoeList.map((shoe, i) => (
          <div className="col-lg-4 col-md-6 mb-4" key={i}>
            <div className="card">
              <img className="card-img-top fit-picture" src={shoe.picture_url} alt={shoe.model_name} />
              <div className="card-body">
                <h5 className="card-title">{shoe.model_name}</h5>
                <p className="card-text">Manufacturer: {shoe.manufacturer}</p>
                <p className="card-text">Color: {shoe.color}</p>
                <p className="card-text">Bin #: {shoe.bin.closet_name}</p>
                <button
                    onClick={deleteShoe}
                    className="btn btn-primary"
                    value={shoe.id}
                  >
                    Delete
                  </button>

              </div>
            </div>
          </div>
        )
        )}
      </div>
    )
  
    
  }

  export default ShoesList;
        
  
  
        
  
