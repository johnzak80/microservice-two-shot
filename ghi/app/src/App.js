import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hats from './hatComponents/Hats'
import ShoesList from './shoesList';
import ShoeForm from './shoeForm';



function App() {
  return (

    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<Hats />} />
          <Route path="/shoes" element={<ShoesList  />}></Route>
          <Route path="shoes/new" element={<ShoeForm />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
