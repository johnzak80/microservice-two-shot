# Wardrobify

Team:

* Person 1 - Michael Zinzun - Hats
* Person 2 - John - Shoes

## To Run:
After cloning the repo run the following commands:
- cd into directory of repo and open docker desktop

- docker-compose build
- docker-compose up


in order to create shoes or hats you need to create a Bin or a Location via the wardrobe API.  We plan on eventually optimizing to be able to add bins and hats via the app. For now, this can be done using a program like insomnia or postman.
 make a post request to http://localhost:8100/api/bins/ and send a JSON object. For example:

    {
        "closet_name": "John's Luxurious Closet",
        "bin_number": "1",
        "bin_size": "10"

    }

You can see the created bin by making a GET request to the same address.

React is being served on http://localhost:3000/, open it up and start making shoes!

You can follow the same procedure to create locations for hats - check the hat model in the Wardrobe API to see the shape of the JSON object to send.




## Project Design
![alt text](/img/Microservices.png "Shoe Diagram")

## Shoes microservice - Port 8080

Shoes - is the model for the shoe microservice. Schema includes: manufacturer, model name, color, and a picture URL. It's foreign key to the BinVO model which is explained below

BinVO - This models includes import_href and closet_name.  You can see the data flow in the diagram below. The Poller.py files calls the wardrobe Api to get all bins, creates a bin via the BinVO model. This function polls every 60 seconds to see if there are new bins in the Wardrobe API.

## Shoe EndPoints
- shoes/ - Lists all shoes in the database
- bins/int:bin_vo_id/shoes/ - This endpoint is used to create a new shoe or list shoes by bin.  You need to create a bin via the Wardrobe API (See optimizations.)
- shoes/int:id/ - deletes shoe by id

![alt text](img/shoes.jpg "Shoe Diagram")

## Hats microservice

Hats is a microservice to the wardode project.  The Hats microservice makes use of the wardobe_api locations endpoints and integrates data with its own database.

![alt text](img/hats.png "Shoe Diagram")


## Optimizations

- Move Hat or Shoe Locations and Bins
- Create Shelves, Locations and Bins via Wardrobe API views
    - Shelf, Location and Bin Views
- Hat and Shoe Detail and Edit views
- Integrate API to create pictures automatically
